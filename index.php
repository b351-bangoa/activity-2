<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Activity 2</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <h1>Divisible by Five</h1>
        <?php
            printDivisibleOfFive();
        ?>

        <h1>Array Manipulation</h1>
        <?php array_push($students, 'John Smith'); ?>
            <p><?php print_r($students); ?></p>
		    <p><?= count($students); ?></p>
		<?php array_push($students, 'Jane Smith'); ?>
            <p><?php print_r($students); ?></p>
		    <p><?= count($students); ?></p>
		<?php array_shift($students); ?>
            <p><?php print_r($students); ?></p>
		    <p><?= count($students); ?></p>




    </body>
</html>